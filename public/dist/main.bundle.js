webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- Fixed navbar -->\n<nav class=\"navbar navbar-default navbar-fixed-top\">\n  <div class=\"container\">\n    <div class=\"navbar-header\">\n      <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">\n        <span class=\"sr-only\">Toggle navigation</span>\n        <span class=\"icon-bar\"></span>\n        <span class=\"icon-bar\"></span>\n        <span class=\"icon-bar\"></span>\n      </button>\n    </div>\n    <div id=\"navbar\" class=\"collapse navbar-collapse\">\n      <ul class=\"nav navbar-nav\">\n\n        <li><a routerLink=\"profile\" routerLinkActive=\"active\">Profile</a></li>\n      </ul>\n      <ul class=\"nav navbar-nav navbar-right\">\n        <li *ngIf=\"!authService.isLoggedIn()\">\n          <a routerLink=\"login\" routerLinkActive=\"active\">Log In</a>\n        </li>\n        <li *ngIf=\"authService.isLoggedIn()\">\n          <ul class=\"menu\">\n            <li>\n              Welcome {{ authService.currentUser.firstname }}\n              <ul>\n                <li routerLink=\"contact\">My Contacts</li>\n                <li routerLink=\"logout\">Logout</li>\n              </ul>\n            </li>\n          </ul>\n        </li>\n      </ul>\n    </div>\n  </div>\n</nav>\n\n<router-outlet></router-outlet>\n\n<footer class=\"footer\">\n  <div class=\"container\">\n    <p class=\"text-muted\">&copy; Copyright 2018 | B M Shams Nahid</p>\n  </div>\n</footer>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user_auth_service__ = __webpack_require__("../../../../../src/app/user/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = (function () {
    function AppComponent(authService) {
        this.authService = authService;
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__user_auth_service__["a" /* AuthService */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_interval__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/interval.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_take__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/take.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_bufferCount__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/bufferCount.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__user_user_module__ = __webpack_require__("../../../../../src/app/user/user.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__home_login_component__ = __webpack_require__("../../../../../src/app/home/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__home_about_component__ = __webpack_require__("../../../../../src/app/home/about.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__common_toastr_service__ = __webpack_require__("../../../../../src/app/common/toastr.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__contact_contact_module__ = __webpack_require__("../../../../../src/app/contact/contact.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_9__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_11__home_login_component__["a" /* LoginComponent */],
                __WEBPACK_IMPORTED_MODULE_12__home_about_component__["a" /* AboutComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["d" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_10__user_user_module__["a" /* UserModule */],
                __WEBPACK_IMPORTED_MODULE_14__contact_contact_module__["a" /* ContactModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* RouterModule */].forRoot([
                    { path: 'login', component: __WEBPACK_IMPORTED_MODULE_11__home_login_component__["a" /* LoginComponent */] },
                    { path: 'about', component: __WEBPACK_IMPORTED_MODULE_12__home_about_component__["a" /* AboutComponent */] },
                    { path: '', redirectTo: 'login', pathMatch: 'full' },
                    { path: '**', redirectTo: 'login', pathMatch: 'full' }
                ])
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_13__common_toastr_service__["a" /* ToastrService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_9__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/common/toastr.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ToastrService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var ToastrService = (function () {
    function ToastrService() {
    }
    ToastrService.prototype.success = function (message, title) {
        toastr.success(message, title);
    };
    ToastrService.prototype.info = function (message, title) {
        toastr.info(message, title);
    };
    ToastrService.prototype.warning = function (message, title) {
        toastr.warning(message, title);
    };
    ToastrService.prototype.error = function (message, title) {
        toastr.error(message, title);
    };
    ToastrService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])()
    ], ToastrService);
    return ToastrService;
}());



/***/ }),

/***/ "../../../../../src/app/contact/contact.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".modal {\n  text-align: center;\n  padding: 0!important;\n}\n\n.modal:before {\n  content: '';\n  display: inline-block;\n  height: 100%;\n  vertical-align: middle;\n  margin-right: -4px;\n}\n\n.modal-dialog {\n  display: inline-block;\n  text-align: left;\n  vertical-align: middle;\n}\n\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/contact/contact.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container pagecontainer\">\n  <div class=\"page-header\">\n    <h1>Contacts List</h1>\n    <button class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#createModal\">Create Contact</button>\n    <button class=\"btn btn-primary\" target=\"_blank\" (click)=\"onclickDownloadAsCSV()\">Download AS CSV</button>\n    <br>\n    <br>\n    <div>\n      <div class=\"input-group add-on\">\n        <input class=\"form-control\" placeholder=\"Search By Address\" name=\"srch-term\" id=\"srch-term\" type=\"text\" [(ngModel)]=\"searchAddressKeyword\">\n        <div class=\"input-group-btn\">\n          <button class=\"btn btn-default\" type=\"submit\" (click)=\"onClickSearchButton()\"><i class=\"glyphicon glyphicon-search\"></i></button>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div>\n    <table class=\"table table-striped\">\n      <thead>\n      <tr>\n        <th>Full Name</th>\n        <th>Nick Name</th>\n        <th>Website</th>\n      </tr>\n      </thead>\n      <tbody>\n      <tr *ngFor=\"let contact of contacts\" data-toggle=\"modal\" data-target=\"#selectModal\" (click)=\"onClickTable(contact)\">\n        <td>{{contact.fullName}}</td>\n        <td>{{contact.nickName}}</td>\n        <td>{{contact.website}}</td>\n      </tr>\n      </tbody>\n    </table>\n\n  </div>\n\n  <!--select from table modal-->\n  <div id=\"selectModal\" class=\"modal fade\" role=\"dialog\" *ngIf=\"currentContact\">\n    <div class=\"modal-dialog modal-sm\">\n      <!-- Modal content-->\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>\n          <h4 class=\"modal-title\" id=\"selectModalLabel\">Contact Information</h4>\n        </div>\n        <div class=\"modal-body\">\n            <div class=\"form-group\">\n              <label for=\"fullName\" class=\"control-label\">Full Name</label>\n              <input type=\"text\" class=\"form-control\" id=\"fullName\" placeholder=\"\" [(ngModel)]=\"currentContact.fullName\">\n            </div>\n            <div class=\"form-group\">\n              <label for=\"nickname\" class=\"control-label\">Nick Name</label>\n              <input type=\"text\" class=\"form-control\" id=\"nickName\" placeholder=\"\" [(ngModel)]=\"currentContact.nickName\">\n            </div>\n            <div class=\"form-group\">\n              <label for=\"website\" class=\"control-label\">Website</label>\n              <input type=\"text\" class=\"form-control\" id=\"website\" placeholder=\"\" [(ngModel)]=\"currentContact.website\">\n            </div>\n            <div class=\"form-group\">\n              <label for=\"address\" class=\"control-label\">Address</label>\n              <input type=\"text\" class=\"form-control\" id=\"address\" placeholder=\"\" [(ngModel)]=\"currentContact.address\">\n            </div>\n            <div class=\"form-group\" *ngFor=\"let pn of currentContact.phoneNumber;let index = index; trackBy:trackByIndex;\">\n              <label for=\"phoneNumber\" class=\"control-label\">Phone {{index + 1}}</label>\n              <input type=\"text\" class=\"form-control\" id=\"phoneNumber\" placeholder=\"\" [(ngModel)]=\"currentContact.phoneNumber[index]\">\n            </div>\n            <div class=\"form-group\">\n              <label for=\"anotherNumber\" class=\"control-label\">Another Number</label>\n              <input type=\"text\" class=\"form-control\" id=\"anotherNumber\" [(ngModel)]=\"anotherNumber\" placeholder=\"\">\n            </div>\n\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n          <button type=\"button\" class=\"btn btn-primary\" (click)=\"onClickUpdateContact()\">Update</button>\n          <button type=\"button\" class=\"btn btn-danger\" (click)=\"onClickDeleteContact()\">Delete</button>\n        </div>\n      </div>\n      </div>\n   </div>\n\n  <!--Create Contact Modal-->\n  <div id=\"createModal\" class=\"modal fade\" role=\"dialog\">\n    <div class=\"modal-dialog modal-sm\">\n      <!-- Modal content-->\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>\n          <h4 class=\"modal-title\" id=\"createModalLabel\">Create Contact</h4>\n        </div>\n        <div class=\"modal-body\">\n          <div class=\"form-group\">\n            <label for=\"newFullName\" class=\"control-label\">Full Name</label>\n            <input type=\"text\" class=\"form-control\" id=\"newFullName\" placeholder=\"\" [(ngModel)]=\"newFullName\">\n          </div>\n          <div class=\"form-group\">\n            <label for=\"newNickname\" class=\"control-label\">Nick Name</label>\n            <input type=\"text\" class=\"form-control\" id=\"newNickName\" placeholder=\"\" [(ngModel)]=\"newNickName\">\n          </div>\n          <div class=\"form-group\">\n            <label for=\"newWebsite\" class=\"control-label\">Website</label>\n            <input type=\"text\" class=\"form-control\" id=\"newWebsite\" placeholder=\"\" [(ngModel)]=\"newWebsite\">\n          </div>\n          <div class=\"form-group\">\n            <label for=\"newNumber\" class=\"control-label\">Phone Number</label>\n            <input type=\"text\" class=\"form-control\" id=\"newNumber\" [(ngModel)]=\"newNumber\" placeholder=\"\">\n          </div>\n          <div class=\"form-group\">\n            <label for=\"newAddress\" class=\"control-label\">Address</label>\n            <input type=\"text\" class=\"form-control\" id=\"newAddress\" [(ngModel)]=\"newAddress\" placeholder=\"\">\n          </div>\n\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n          <button type=\"button\" class=\"btn btn-primary\" (click)=\"onClickCreateContact()\">Create</button>\n        </div>\n      </div>\n    </div>\n  </div>\n\n</div>\n\n\n"

/***/ }),

/***/ "../../../../../src/app/contact/contact.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user_auth_service__ = __webpack_require__("../../../../../src/app/user/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact_service__ = __webpack_require__("../../../../../src/app/contact/contact.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__common_toastr_service__ = __webpack_require__("../../../../../src/app/common/toastr.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__contact__ = __webpack_require__("../../../../../src/app/contact/contact.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ContactComponent = (function () {
    function ContactComponent(authService, contactService, fb, route, router, toastr, datePipe) {
        this.authService = authService;
        this.contactService = contactService;
        this.fb = fb;
        this.route = route;
        this.router = router;
        this.toastr = toastr;
        this.datePipe = datePipe;
        this.phoneNumber = [];
    }
    ContactComponent.prototype.ngOnInit = function () {
        this.currentContact = null;
        this.userObj = this.authService.currentUser;
        console.log(this.userObj);
        this.getAllContacts();
    };
    ContactComponent.prototype.onClickCreateContact = function () {
        var _this = this;
        console.log(this.newFullName);
        console.log(this.newNickName);
        console.log(this.newWebsite);
        console.log(this.newNumber);
        console.log(this.newAddress);
        this.newNumber = parseInt(this.newNumber);
        if (!this.newFullName || !this.newNickName || !this.newWebsite || !this.newNumber || !this.newAddress) {
            this.toastr.warning('Invalid or Incomplete Information');
            return;
        }
        if (this.newFullName.length <= 0 || this.newNickName.length <= 0 || this.newWebsite.length <= 0 || this.newNumber <= 0 || this.newAddress.length <= 0) {
            this.toastr.warning('Invalid or Incomplete Information');
            return;
        }
        var contact = new __WEBPACK_IMPORTED_MODULE_7__contact__["a" /* Contact */]();
        contact.fullName = this.newFullName;
        contact.nickName = this.newNickName;
        contact.website = this.newWebsite;
        contact.phoneNumber = this.newNumber;
        contact.address = this.newAddress;
        // contact.phoneNumber.push(this.newNumber);
        this.contactService.createContact(this.userObj.user.userid, contact).subscribe(function (data) {
            if (data.success == true) {
                _this.toastr.success('New contact created');
                console.log(data);
                _this.contacts.push(data.contact);
                _this.newFullName = null;
                _this.newNickName = null;
                _this.newWebsite = null;
                _this.phoneNumber = null;
                _this.newAddress = null;
            }
            else {
                _this.toastr.error('Error in creating new contact: ' + data.message);
            }
        });
    };
    ContactComponent.prototype.getAllContacts = function () {
        var _this = this;
        console.log('UserID: ' + this.userObj.user.userid);
        this.contactService.getAllContact(this.userObj.user.userid).subscribe(function (data) {
            console.log(data);
            if (data.success == false) {
                _this.toastr.error(data.message);
            }
            else {
                _this.contacts = data.contacts;
                console.log(_this.contacts);
            }
        });
    };
    ContactComponent.prototype.onClickTable = function (contact) {
        this.currentContact = contact;
    };
    ContactComponent.prototype.onClickUpdateContact = function () {
        var _this = this;
        if (this.checkValidation(this.currentContact) == false) {
            this.toastr.warning('Invalid or Incomplete Information');
            console.log(this.currentContact);
        }
        else {
            this.contactService.updateContact(this.userObj.user.userid, this.currentContact._id, this.currentContact).subscribe(function (data) {
                console.log(data);
                if (data.success == true) {
                    _this.toastr.success('Contacts Updated Successfully');
                }
                else {
                    _this.toastr.error(data.message);
                }
            });
        }
    };
    ContactComponent.prototype.onClickDeleteContact = function () {
        var _this = this;
        this.contactService.deleteContact(this.userObj.user.userid, this.currentContact._id).subscribe(function (data) {
            if (data.success == true) {
                _this.toastr.success('Successfully removed the contact.');
                var updatedContactList = [];
                _this.contacts.map(function (contact) {
                    if (contact._id != _this.currentContact._id) {
                        updatedContactList.push(contact);
                    }
                });
                _this.contacts = updatedContactList;
            }
            else {
                _this.toastr.error('Error in deleting the contact: ' + data.message);
            }
        });
    };
    ContactComponent.prototype.onClickSearchButton = function () {
        var _this = this;
        console.log('Searching: ' + this.searchAddressKeyword);
        if (!this.searchAddressKeyword) {
            this.toastr.warning('Invalid Search Keyword');
            return;
        }
        else {
            var newFilteredContactsList = [];
            this.contacts.map(function (contact) {
                if (contact.address == _this.searchAddressKeyword) {
                    newFilteredContactsList.push(contact);
                }
            });
            if (newFilteredContactsList.length <= 0) {
                this.toastr.warning('No contacts found.');
            }
            else {
                this.contacts = newFilteredContactsList;
            }
        }
    };
    ContactComponent.prototype.onclickDownloadAsCSV = function () {
        console.log('Download as CSV');
        this.contactService.downloadCsv(this.userObj.user.userid).subscribe(function (data) {
            console.log(data);
            var parsedResponse = data.text();
            var blob = new Blob([parsedResponse], { type: 'text/csv' });
            var url = window.URL.createObjectURL(blob);
            if (navigator.msSaveOrOpenBlob) {
                navigator.msSaveBlob(blob, 'Book.csv');
            }
            else {
                var a = document.createElement('a');
                a.href = url;
                a.download = 'contacts.csv';
                document.body.appendChild(a);
                a.click();
                document.body.removeChild(a);
            }
            window.URL.revokeObjectURL(url);
        });
    };
    ContactComponent.prototype.checkValidation = function (contact) {
        var _this = this;
        if (!contact.fullName || !contact.nickName || !contact.website || contact.phoneNumber.length <= 0 || !contact.address) {
            this.toastr.warning('Incomplete or invalid information, undefined value');
            return false;
        }
        if (this.anotherNumber) {
            try {
                var newNumber = parseInt(this.anotherNumber);
                this.currentContact.phoneNumber.push(newNumber);
                this.anotherNumber = null;
            }
            catch (err) {
                this.toastr.warning('New number can not be added: ' + err);
            }
        }
        var validPhoneNumber = true;
        var filteredContactNumberList = [];
        contact.phoneNumber.map(function (value) {
            try {
                if (value != null && value.toString().length > 0 && parseInt(value)) {
                    filteredContactNumberList.push(parseInt(value));
                }
            }
            catch (err) {
                validPhoneNumber = false;
                _this.toastr.warning('Invalid Phone Number: ' + value + ' is not a number');
            }
        });
        this.currentContact.phoneNumber = filteredContactNumberList;
        return validPhoneNumber;
    };
    ContactComponent.prototype.trackByIndex = function (index, obj) {
        return index;
    };
    ContactComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("../../../../../src/app/contact/contact.component.html"),
            styles: [__webpack_require__("../../../../../src/app/contact/contact.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__user_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_2__contact_service__["a" /* ContactService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_5__common_toastr_service__["a" /* ToastrService */],
            __WEBPACK_IMPORTED_MODULE_6__angular_common__["d" /* DatePipe */]])
    ], ContactComponent);
    return ContactComponent;
}());



/***/ }),

/***/ "../../../../../src/app/contact/contact.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user_auth_guard_service__ = __webpack_require__("../../../../../src/app/user/auth-guard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__contact_component__ = __webpack_require__("../../../../../src/app/contact/contact.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__user_auth_service__ = __webpack_require__("../../../../../src/app/user/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__contact_service__ = __webpack_require__("../../../../../src/app/contact/contact.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var ContactModule = (function () {
    function ContactModule() {
    }
    ContactModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["d" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* RouterModule */].forChild([
                    { path: 'contact', canActivate: [__WEBPACK_IMPORTED_MODULE_4__user_auth_guard_service__["a" /* AuthGuard */]], component: __WEBPACK_IMPORTED_MODULE_5__contact_component__["a" /* ContactComponent */] }
                ])
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__contact_component__["a" /* ContactComponent */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__angular_common__["d" /* DatePipe */],
                __WEBPACK_IMPORTED_MODULE_7__user_auth_service__["a" /* AuthService */],
                __WEBPACK_IMPORTED_MODULE_4__user_auth_guard_service__["a" /* AuthGuard */],
                __WEBPACK_IMPORTED_MODULE_8__contact_service__["a" /* ContactService */]
            ]
        })
    ], ContactModule);
    return ContactModule;
}());



/***/ }),

/***/ "../../../../../src/app/contact/contact.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_do__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/do.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_observable_throw__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/throw.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ContactService = (function () {
    function ContactService(http) {
        this.http = http;
        var theUser = JSON.parse(localStorage.getItem('currentUser'));
        if (theUser) {
            this.jwtToken = theUser.token;
        }
    }
    ContactService.prototype.createContact = function (userId, contact) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', "" + this.jwtToken);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.post("https://mean-address-book.herokuapp.com/api/contact/" + userId, JSON.stringify(contact), options)
            .map(function (response) {
            return response.json();
        })
            .catch(this.handleError);
    };
    ContactService.prototype.getSingleContact = function (userId, contactId) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', "" + this.jwtToken);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        this.http.get('https://mean-address-book.herokuapp.com/api/contact/${userId}/${contactId}', options)
            .map(function (response) {
            return response.json();
        })
            .catch(this.handleError);
    };
    ContactService.prototype.getAllContact = function (userId) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', "" + this.jwtToken);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.get("https://mean-address-book.herokuapp.com/api/contact/" + userId, options)
            .map(function (response) {
            return response.json();
        })
            .catch(this.handleError);
    };
    ContactService.prototype.updateContact = function (userId, contactId, contact) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', "" + this.jwtToken);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.put("https://mean-address-book.herokuapp.com/api/contact/" + userId + "/" + contactId, JSON.stringify(contact), options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ContactService.prototype.deleteContact = function (userId, contactId) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', "" + this.jwtToken);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.delete("https://mean-address-book.herokuapp.com/api/contact/" + userId + "/" + contactId, options)
            .map(function (response) {
            return response.json();
        })
            .catch(this.handleError);
    };
    ContactService.prototype.downloadCsv = function (userId) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', "" + this.jwtToken);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.get("https://mean-address-book.herokuapp.com/api/csv-generator/" + userId, options)
            .map(function (response) {
            return response;
        })
            .catch(this.handleError);
    };
    ContactService.prototype.handleError = function (error) {
        if (error) {
            console.log('Error in Contact Service: ' + error);
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw(error.json().error || 'Server Error');
        }
        else {
            console.log('Unknown err');
        }
    };
    ContactService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], ContactService);
    return ContactService;
}());



/***/ }),

/***/ "../../../../../src/app/contact/contact.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Contact; });
var Contact = (function () {
    function Contact() {
    }
    return Contact;
}());



/***/ }),

/***/ "../../../../../src/app/home/about.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container pagecontainer\">\n  <div class=\"page-header\">\n    <h1>About</h1>\n  </div>\n\n  <div class=\"jumbotron\">\n    <h1>Expense Watch</h1>\n    <p>A sample MEAN app which can be used as a starting template for your real world Apps!</p>\n  </div>\n\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/home/about.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AboutComponent = (function () {
    function AboutComponent() {
    }
    AboutComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("../../../../../src/app/home/about.component.html")
        })
    ], AboutComponent);
    return AboutComponent;
}());



/***/ }),

/***/ "../../../../../src/app/home/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container pagecontainer\">\n\n  <form [formGroup]=\"loginForm\" (ngSubmit)=\"loginUser(loginForm.value)\" class=\"form-signin\" novalidate autocomplete=\"off\">\n    <h2>Please sign in</h2>\n    <label for=\"inputUser\" class=\"sr-only\">Username</label>\n    <input type=\"text\" id=\"inputUser\" class=\"form-control\" [formControl]=\"username\" placeholder=\"Username\" autofocus>\n    <div class=\"text-danger\" *ngIf=\"loginForm.get('username').dirty && loginForm.get('username').invalid\">\n      Please enter username\n    </div>\n    <label for=\"inputPassword\" class=\"sr-only\">Password</label>\n    <input type=\"password\" id=\"inputPassword\" class=\"form-control\" [formControl]=\"password\" placeholder=\"Password\">\n    <div class=\"text-danger\" *ngIf=\"loginForm.get('password').dirty && loginForm.get('password').invalid\">\n      Please enter password\n    </div>\n    <br><br>\n    <button class=\"btn btn-lg btn-primary btn-block\" type=\"submit\" [disabled]=\"loginForm.invalid\" >Sign in</button>\n    <br>\n    <div class=\"row\">\n      <div class=\"col-md-6 col-sm-6 col-xs-6\"></div>\n      <div class=\"col-md-6 col-sm-6 col-xs-6\" style=\"text-align: right\"><a [routerLink]=\"['/register']\">Register</a></div>\n    </div>\n  </form>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/home/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user_auth_service__ = __webpack_require__("../../../../../src/app/user/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__common_toastr_service__ = __webpack_require__("../../../../../src/app/common/toastr.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginComponent = (function () {
    function LoginComponent(fb, authService, router, toastr) {
        this.fb = fb;
        this.authService = authService;
        this.router = router;
        this.toastr = toastr;
        this.username = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required]);
        this.password = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required]);
        this.loginForm = this.fb.group({
            username: this.username,
            password: this.password
        });
    }
    LoginComponent.prototype.loginUser = function (formdata) {
        var _this = this;
        if (this.loginForm.dirty && this.loginForm.value) {
            this.authService.login(this.loginForm.value)
                .subscribe(function (data) {
                if (data.json().success === false) {
                    _this.toastr.error(data.json().message);
                }
                else {
                    _this.toastr.success('Login Successfull');
                    _this.router.navigate(['contact']);
                }
                _this.loginForm.reset();
            });
        }
    };
    LoginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("../../../../../src/app/home/login.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_2__user_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_4__common_toastr_service__["a" /* ToastrService */]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "../../../../../src/app/user/auth-guard.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_service__ = __webpack_require__("../../../../../src/app/user/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__common_toastr_service__ = __webpack_require__("../../../../../src/app/common/toastr.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthGuard = (function () {
    function AuthGuard(authService, router, toastr) {
        this.authService = authService;
        this.router = router;
        this.toastr = toastr;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        return this.checkLoggedIn(state.url);
    };
    AuthGuard.prototype.checkLoggedIn = function (url) {
        if (this.authService.isLoggedIn()) {
            return true;
        }
        this.toastr.info("Please login to access this page.");
        this.router.navigate(['/login']);
        return false;
    };
    AuthGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_3__common_toastr_service__["a" /* ToastrService */]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "../../../../../src/app/user/auth.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_do__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/do.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_observable_throw__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/throw.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AuthService = (function () {
    function AuthService(http) {
        this.http = http;
        this.loginUrl = 'https://mean-address-book.herokuapp.com/login';
    }
    AuthService.prototype.isLoggedIn = function () {
        try {
            var theUser = JSON.parse(localStorage.getItem('currentUser'));
            if (theUser) {
                this.currentUser = theUser;
                return true;
            }
        }
        catch (e) {
            return false;
        }
        return false;
    };
    AuthService.prototype.login = function (oUser) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.post('https://mean-address-book.herokuapp.com/login', JSON.stringify(oUser), options)
            .do(function (response) {
            if (response.json().success) {
                _this.currentUser = response.json().message;
                var userObj = {};
                userObj.user = response.json().message;
                userObj.token = response.json().token;
                localStorage.setItem('currentUser', JSON.stringify(userObj));
            }
            response.json();
        })
            .catch(this.handleError);
    };
    AuthService.prototype.logout = function () {
        this.currentUser = null;
        localStorage.removeItem('currentUser');
    };
    AuthService.prototype.handleError = function (error) {
        console.log('Error is: ' + error);
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw(error.json().error || 'Server Error');
    };
    AuthService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "../../../../../src/app/user/logout.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LogoutComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__common_toastr_service__ = __webpack_require__("../../../../../src/app/common/toastr.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_service__ = __webpack_require__("../../../../../src/app/user/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LogoutComponent = (function () {
    function LogoutComponent(authService, router, toastr) {
        this.authService = authService;
        this.router = router;
        this.toastr = toastr;
    }
    LogoutComponent.prototype.ngOnInit = function () {
        this.authService.logout();
        this.toastr.success('You have successfully logged out.');
        this.router.navigate(['login']);
    };
    LogoutComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: ''
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_2__common_toastr_service__["a" /* ToastrService */]])
    ], LogoutComponent);
    return LogoutComponent;
}());



/***/ }),

/***/ "../../../../../src/app/user/password.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container pagecontainer\">\n    <div class=\"page-header\">\n        <h1>Change Password</h1>\n    </div>\n\n    <form [formGroup]=\"passwordForm\" (ngSubmit)=\"updatePassword(passwordForm.value)\" class=\"form-horizontal\" novalidate autocomplete=\"off\">\n        <div class=\"form-group\" [ngClass]=\"{'has-error': passwordForm.get('oldpassword').invalid && passwordForm.get('oldpassword').dirty}\">\n            <label for=\"oldpassword\" class=\"col-sm-2 control-label\">Existing Password</label>\n            <div class=\"input-group col-sm-10\">\n                <div class=\"input-group-addon\"><i class=\"glyphicon glyphicon glyphicon-lock\"></i></div>\n                <input type=\"password\" class=\"form-control\" id=\"oldpassword\" [formControl]=\"oldpassword\" placeholder=\"Existing Password\">\n            </div>\n            <div class=\"text-danger\" *ngIf=\"passwordForm.get('oldpassword').dirty && passwordForm.get('oldpassword').errors\">\n                <span class=\"col-sm-2\"></span>\n                <span class=\"col-sm-10\" style=\"padding:4px 0 0\" *ngIf=\"passwordForm.get('oldpassword').errors.required\">\n                    Please enter existing password\n                </span>\n            </div>\n        </div>\n\n        <div formGroupName=\"passwordGroup\" [ngClass]=\"{'has-error': passwordForm.get('passwordGroup').errors }\">\n\n            <div class=\"form-group\" [ngClass]=\"{'has-error': passwordForm.get('passwordGroup.password').invalid && passwordForm.get('passwordGroup.password').dirty}\">\n                <label for=\"password\" class=\"col-sm-2 control-label\">New Password</label>\n                <div class=\"input-group col-sm-10\">\n                    <div class=\"input-group-addon\"><i class=\"glyphicon glyphicon glyphicon-asterisk\"></i></div>\n                    <input type=\"password\" class=\"form-control\" id=\"password\" [formControl]=\"password\" placeholder=\"New Pasword\">\n                </div>\n                <div class=\"text-danger\"  *ngIf=\"passwordForm.get('passwordGroup.password').dirty && passwordForm.get('passwordGroup.password').errors\">\n                    <span class=\"col-sm-2\"></span>\n                    <span class=\"col-sm-10\" style=\"padding:4px 0 0\" *ngIf=\"passwordForm.get('passwordGroup.password').errors.required\">\n                        Please enter password\n                    </span>\n                    <span class=\"col-sm-10\" style=\"padding:4px 0 0\" *ngIf=\"passwordForm.get('passwordGroup.password').errors.pattern\">\n                        Password must contain one letter, number & special characters\n                    </span>\n                </div>\n            </div>\n            <div class=\"form-group\" [ngClass]=\"{'has-error': passwordForm.get('passwordGroup.retypepass').invalid && passwordForm.get('passwordGroup.retypepass').dirty}\">\n                <label for=\"retypepass\" class=\"col-sm-2 control-label\">Retype Password <sup>*</sup></label>\n                <div class=\"input-group col-sm-10\">\n                    <div class=\"input-group-addon\"><i class=\"glyphicon glyphicon glyphicon-asterisk\"></i></div>\n                    <input type=\"password\" class=\"form-control\" [formControl]=\"retypepass\" id=\"retypepass\" placeholder=\"Retype Password\">\n                </div>\n                <div class=\"text-danger\" *ngIf=\"(passwordForm.get('passwordGroup.retypepass').touched || passwordForm.get('passwordGroup.retypepass').dirty) && (passwordForm.get('passwordGroup.retypepass').errors || passwordForm.get('passwordGroup').errors)\">\n                    <span class=\"col-sm-2\"></span>\n                    <span class=\"col-sm-10\" style=\"padding:4px 0 0\" *ngIf=\"passwordForm.get('passwordGroup.retypepass').errors?.required\">\n                        Please confirm your password\n                    </span>\n                    <span class=\"col-sm-10\" style=\"padding:4px 0 0\" *ngIf=\"passwordForm.get('passwordGroup').errors?.mismatchedPassword\">\n                        Password do not match\n                    </span>\n                </div>\n            </div>\n\n        </div>\n    \n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\"></label>\n            <div class=\"col-sm-10\">\n                <button type=\"submit\" [disabled]=\"passwordForm.invalid\" class=\"btn btn-primary\">Update</button>\n                <button type=\"reset\" class=\"btn btn-default\">Cancel</button>\n            </div>\n        </div>\n    </form>\n\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/user/password.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PasswordComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__common_toastr_service__ = __webpack_require__("../../../../../src/app/common/toastr.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user_user_service__ = __webpack_require__("../../../../../src/app/user/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__user_auth_service__ = __webpack_require__("../../../../../src/app/user/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






function comparePassword(c) {
    var passwordControl = c.get('password');
    var confirmControl = c.get('retypepass');
    if (passwordControl.pristine || confirmControl.pristine) {
        return null;
    }
    if (passwordControl.value === confirmControl.value) {
        return null;
    }
    return { 'mismatchedPassword': true };
}
var PasswordComponent = (function () {
    function PasswordComponent(fb, authService, userService, router, toastr) {
        this.fb = fb;
        this.authService = authService;
        this.userService = userService;
        this.router = router;
        this.toastr = toastr;
        this.oldpassword = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required]);
        this.password = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required]);
        this.retypepass = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required]);
    }
    PasswordComponent.prototype.ngOnInit = function () {
        this.userObj = this.authService.currentUser;
        this.passwordForm = this.fb.group({
            oldpassword: this.oldpassword,
            passwordGroup: this.fb.group({
                password: this.password,
                retypepass: this.retypepass,
            }, { validator: comparePassword })
        });
    };
    PasswordComponent.prototype.updatePassword = function (formdata) {
        var _this = this;
        if (this.passwordForm.dirty && this.passwordForm.valid) {
            var theForm = this.passwordForm.value;
            var thePass = this.passwordForm.value.passwordGroup.password;
            theForm.password = thePass;
            delete theForm.passwordGroup;
            this.userService.updatePassword(this.userObj.user.userid, theForm)
                .subscribe(function (data) {
                if (data.success === false) {
                    if (data.errcode) {
                        _this.authService.logout();
                        _this.router.navigate(['login']);
                    }
                    _this.toastr.error(data.message);
                }
                else {
                    _this.toastr.success(data.message);
                }
                _this.passwordForm.reset();
            });
        }
    };
    PasswordComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("../../../../../src/app/user/password.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_5__user_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_4__user_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_3__common_toastr_service__["a" /* ToastrService */]])
    ], PasswordComponent);
    return PasswordComponent;
}());



/***/ }),

/***/ "../../../../../src/app/user/profile.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container pagecontainer\">\n    <div class=\"page-header\">\n        <h1>My Profile</h1>\n    </div>\n\n    <form [formGroup]=\"profileForm\" (ngSubmit)=\"updateUser(profileForm.value)\" class=\"form-horizontal\" novalidate autocomplete=\"off\">\n\n        <div class=\"form-group\" [ngClass]=\"{'has-error': profileForm.get('username').invalid && profileForm.get('username').dirty}\">\n          <label for=\"uname\" class=\"col-sm-2 control-label\">User Name</label>\n          <div class=\"input-group col-sm-10\">\n            <div class=\"input-group-addon\"><i class=\"glyphicon glyphicon-user\"></i></div>\n            <input type=\"text\" class=\"form-control\" id=\"uname\" [formControl]=\"username\" placeholder=\"User Name\">\n          </div>\n          <div class=\"text-danger\" *ngIf=\"profileForm.get('username').dirty && profileForm.get('username').invalid\">\n            <span class=\"col-sm-2\"></span>\n            <span class=\"col-sm-10\" style=\"padding:4px 0 0\">Please enter your user name</span>\n          </div>\n        </div>\n\n        <div class=\"form-group\" [ngClass]=\"{'has-error': profileForm.get('firstname').invalid && profileForm.get('firstname').dirty}\">\n            <label for=\"fname\" class=\"col-sm-2 control-label\">First Name</label>\n            <div class=\"input-group col-sm-10\">\n                <div class=\"input-group-addon\"><i class=\"glyphicon glyphicon-font\"></i></div>\n                <input type=\"text\" class=\"form-control\" id=\"fname\" [formControl]=\"firstname\" placeholder=\"First Name\">\n            </div>\n            <div class=\"text-danger\" *ngIf=\"profileForm.get('firstname').dirty && profileForm.get('firstname').invalid\">\n                <span class=\"col-sm-2\"></span>\n                <span class=\"col-sm-10\" style=\"padding:4px 0 0\">Please enter your first name</span>\n            </div>\n        </div>\n        <div class=\"form-group\" [ngClass]=\"{'has-error': profileForm.get('lastname').invalid && profileForm.get('lastname').dirty}\">\n            <label for=\"lname\" class=\"col-sm-2 control-label\">Last Name</label>\n            <div class=\"input-group col-sm-10\">\n                <div class=\"input-group-addon\"><i class=\"glyphicon glyphicon glyphicon-bold\"></i></div>\n                <input type=\"text\" class=\"form-control\" id=\"lname\" [formControl]=\"lastname\" placeholder=\"Last Name\">\n            </div>\n            <div class=\"text-danger\" *ngIf=\"profileForm.get('lastname').dirty && profileForm.get('lastname').invalid\">\n                <span class=\"col-sm-2\"></span>\n                <span class=\"col-sm-10\" style=\"padding:4px 0 0\">Please enter your last name</span>\n            </div>\n        </div>\n        <div class=\"form-group\" [ngClass]=\"{'has-error': profileForm.get('email').invalid && profileForm.get('email').dirty}\">\n            <label for=\"lname\" class=\"col-sm-2 control-label\">Email</label>\n            <div class=\"input-group col-sm-10\">\n                <div class=\"input-group-addon\">@</div>\n                <input type=\"text\" class=\"form-control\" id=\"lname\" [formControl]=\"email\" placeholder=\"Email\">\n            </div>\n            <div class=\"text-danger\" *ngIf=\"profileForm.get('email').dirty && profileForm.get('email').invalid\">\n                <span class=\"col-sm-2\"></span>\n                <span class=\"col-sm-10\" style=\"padding:4px 0 0\">Please enter a valid email</span>\n            </div>\n        </div>\n\n        <div class=\"form-group\">\n            <label for=\"password\" class=\"col-sm-2 control-label\">Last Login</label>\n            <div class=\"input-group col-sm-10\">\n                <div class=\"input-group-addon\"><i class=\"glyphicon glyphicon glyphicon-calendar\"></i></div>\n                <div class=\"form-ele\">{{ userObj.lastlogin | date:'medium' }}</div>\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label class=\"col-sm-2 control-label\"></label>\n            <div class=\"col-sm-10\">\n                <button type=\"submit\" [disabled]=\"profileForm.invalid\" class=\"btn btn-primary\">Update</button>\n                <button type=\"reset\" class=\"btn btn-default\">Cancel</button>\n            </div>\n        </div>\n    </form>\n\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/user/profile.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__common_toastr_service__ = __webpack_require__("../../../../../src/app/common/toastr.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user_user_service__ = __webpack_require__("../../../../../src/app/user/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__user_auth_service__ = __webpack_require__("../../../../../src/app/user/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ProfileComponent = (function () {
    function ProfileComponent(fb, authService, userService, router, toastr) {
        this.fb = fb;
        this.authService = authService;
        this.userService = userService;
        this.router = router;
        this.toastr = toastr;
        this.username = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required]);
        this.firstname = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required]);
        this.lastname = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required]);
        this.email = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].email]);
    }
    ProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.userObj = this.authService.currentUser;
        console.log('User obj: ');
        console.log(this.userObj);
        this.profileForm = this.fb.group({
            username: this.username,
            firstname: this.firstname,
            lastname: this.lastname,
            email: this.email
        });
        this.userService.getUser(this.userObj.user.userid).subscribe(function (data) {
            if (data.success === false) {
                if (data.errcode) {
                    _this.authService.logout();
                    _this.router.navigate(['login']);
                }
                _this.toastr.error(data.message);
            }
            else {
                _this.user = data.data[0];
                _this.populateForm(_this.user);
            }
        });
    };
    ProfileComponent.prototype.populateForm = function (data) {
        this.profileForm.patchValue({
            username: data.username,
            firstname: data.firstname,
            lastname: data.lastname,
            email: data.email,
        });
    };
    ProfileComponent.prototype.updateUser = function (formdata) {
        var _this = this;
        if (this.profileForm.dirty && this.profileForm.valid) {
            this.userService.updateUser(this.userObj.user.userid, this.profileForm.value)
                .subscribe(function (data) {
                if (data.success === false) {
                    if (data.errcode) {
                        _this.authService.logout();
                        _this.router.navigate(['login']);
                    }
                    _this.toastr.error(data.message);
                }
                else {
                    _this.toastr.success(data.message);
                    var theUser = JSON.parse(localStorage.getItem('currentUser'));
                    theUser.user.firstname = _this.profileForm.value.firstname;
                    localStorage.setItem('currentUser', JSON.stringify(theUser));
                }
            });
        }
    };
    ProfileComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("../../../../../src/app/user/profile.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_5__user_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_4__user_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_3__common_toastr_service__["a" /* ToastrService */]])
    ], ProfileComponent);
    return ProfileComponent;
}());



/***/ }),

/***/ "../../../../../src/app/user/register.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container pagecontainer\">\n  <div class=\"page-header\">\n    <h1>Register</h1>\n  </div>\n\n  <form [formGroup]=\"registerForm\" (ngSubmit)=\"registerUser(registerForm.value)\" class=\"form-horizontal\" novalidate autocomplete=\"off\">\n    <div class=\"form-group\" [ngClass]=\"{'has-error': registerForm.get('firstname').invalid && registerForm.get('firstname').dirty}\">\n      <label for=\"fname\" class=\"col-sm-2 control-label\">First Name <sup>*</sup></label>\n      <div class=\"input-group col-sm-10\">\n        <div class=\"input-group-addon\"><i class=\"glyphicon glyphicon-font\"></i></div>\n        <input type=\"text\" class=\"form-control\" [formControl]=\"firstname\" id=\"fname\" placeholder=\"First Name\">\n      </div>\n      <div class=\"text-danger\" *ngIf=\"registerForm.get('firstname').dirty && registerForm.get('firstname').errors\">\n        <span class=\"col-sm-2\"></span>\n        <span class=\"col-sm-10\" style=\"padding:4px 0 0\" *ngIf=\"registerForm.get('firstname').errors.required\">\n                    Please enter your first name\n                </span>\n      </div>\n    </div>\n    <div class=\"form-group\" [ngClass]=\"{'has-error': registerForm.get('lastname').invalid && registerForm.get('lastname').dirty}\">\n      <label for=\"lname\" class=\"col-sm-2 control-label\">Last Name <sup>*</sup></label>\n      <div class=\"input-group col-sm-10\">\n        <div class=\"input-group-addon\"><i class=\"glyphicon glyphicon glyphicon-bold\"></i></div>\n        <input type=\"text\" class=\"form-control\" [formControl]=\"lastname\" id=\"lname\" placeholder=\"Last Name\">\n      </div>\n      <div class=\"text-danger\" *ngIf=\"registerForm.get('lastname').dirty && registerForm.get('lastname').errors\">\n        <span class=\"col-sm-2\"></span>\n        <span class=\"col-sm-10\" style=\"padding:4px 0 0\" *ngIf=\"registerForm.get('lastname').errors.required\">\n                    Please enter your last name\n                </span>\n      </div>\n    </div>\n    <div class=\"form-group\" [ngClass]=\"{'has-error': registerForm.get('email').invalid && registerForm.get('email').dirty}\">\n      <label for=\"email\" class=\"col-sm-2 control-label\">Email <sup>*</sup></label>\n      <div class=\"input-group col-sm-10\">\n        <div class=\"input-group-addon\">@</div>\n        <input type=\"text\" class=\"form-control\" [formControl]=\"email\" id=\"email\" placeholder=\"Email\">\n      </div>\n      <div class=\"text-danger\" *ngIf=\"registerForm.get('email').dirty && registerForm.get('email').errors\">\n        <span class=\"col-sm-2\"></span>\n        <span class=\"col-sm-10\" style=\"padding:4px 0 0\" *ngIf=\"registerForm.get('email').invalid\">\n                    Please enter a valid email\n                </span>\n      </div>\n    </div>\n    <div class=\"form-group\" [ngClass]=\"{'has-error': registerForm.get('username').invalid && registerForm.get('username').dirty}\">\n      <label for=\"username\" class=\"col-sm-2 control-label\">Username <sup>*</sup></label>\n      <div class=\"input-group col-sm-10\">\n        <div class=\"input-group-addon\"><i class=\"glyphicon glyphicon glyphicon-user\"></i></div>\n        <input type=\"text\" class=\"form-control\" [formControl]=\"username\" id=\"username\" placeholder=\"Username\">\n      </div>\n      <div class=\"text-danger\" *ngIf=\"registerForm.get('username').dirty && registerForm.get('username').errors\">\n        <span class=\"col-sm-2\"></span>\n        <span class=\"col-sm-10\" style=\"padding:4px 0 0\" *ngIf=\"registerForm.get('username').errors.required\">\n                    Please enter username\n                </span>\n        <span class=\"col-sm-10\" style=\"padding:4px 0 0\" *ngIf=\"registerForm.get('username').errors.minlength\">\n                    Username must be longer than 3 characters.\n                </span>\n        <span class=\"col-sm-10\" style=\"padding:4px 0 0\" *ngIf=\"registerForm.get('username').errors.maxlength\">\n                    Username cannot be longer than 16 characters.\n                </span>\n      </div>\n    </div>\n\n    <div formGroupName=\"passwordGroup\" [ngClass]=\"{'has-error': registerForm.get('passwordGroup').errors }\">\n\n      <div class=\"form-group\" [ngClass]=\"{'has-error': registerForm.get('passwordGroup.password').invalid && registerForm.get('passwordGroup.password').dirty}\">\n        <label for=\"password\" class=\"col-sm-2 control-label\">Password <sup>*</sup></label>\n        <div class=\"input-group col-sm-10\">\n          <div class=\"input-group-addon\"><i class=\"glyphicon glyphicon glyphicon-asterisk\"></i></div>\n          <input type=\"password\" class=\"form-control\" [formControl]=\"password\" id=\"password\" placeholder=\"Pasword\">\n        </div>\n        <div class=\"text-danger\"  *ngIf=\"registerForm.get('passwordGroup.password').dirty && registerForm.get('passwordGroup.password').errors\">\n          <span class=\"col-sm-2\"></span>\n          <span class=\"col-sm-10\" style=\"padding:4px 0 0\" *ngIf=\"registerForm.get('passwordGroup.password').errors.required\">\n                        Please enter password\n                    </span>\n          <span class=\"col-sm-10\" style=\"padding:4px 0 0\" *ngIf=\"registerForm.get('passwordGroup.password').errors.pattern\">\n                        Password must contain one letter, number & special characters\n                    </span>\n        </div>\n\n      </div>\n      <div class=\"form-group\" [ngClass]=\"{'has-error': registerForm.get('passwordGroup.retypepass').invalid && registerForm.get('passwordGroup.retypepass').dirty}\">\n        <label for=\"retypepass\" class=\"col-sm-2 control-label\">Retype Password <sup>*</sup></label>\n        <div class=\"input-group col-sm-10\">\n          <div class=\"input-group-addon\"><i class=\"glyphicon glyphicon glyphicon-asterisk\"></i></div>\n          <input type=\"password\" class=\"form-control\" [formControl]=\"retypepass\" id=\"retypepass\" placeholder=\"Retype Password\">\n        </div>\n        <div class=\"text-danger\" *ngIf=\"(registerForm.get('passwordGroup.retypepass').touched || registerForm.get('passwordGroup.retypepass').dirty) && (registerForm.get('passwordGroup.retypepass').errors || registerForm.get('passwordGroup').errors)\">\n          <span class=\"col-sm-2\"></span>\n          <span class=\"col-sm-10\" style=\"padding:4px 0 0\" *ngIf=\"registerForm.get('passwordGroup.retypepass').errors?.required\">\n                        Please confirm your password\n                    </span>\n          <span class=\"col-sm-10\" style=\"padding:4px 0 0\" *ngIf=\"registerForm.get('passwordGroup').errors?.mismatchedPassword\">\n                        Password do not match\n                    </span>\n        </div>\n      </div>\n\n    </div>\n\n    <div class=\"form-group\">\n      <label class=\"col-sm-2 control-label\"></label>\n      <div class=\"col-sm-5\">\n        <button type=\"submit\" [disabled]=\"registerForm.invalid\" class=\"btn btn-primary\">Submit</button>\n        <button type=\"reset\" class=\"btn btn-default\">Reset</button>\n      </div>\n      <div class=\"col-sm-5\" style=\"text-align:right\">\n        <sup>*</sup> required\n      </div>\n    </div>\n  </form>\n\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/user/register.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__common_toastr_service__ = __webpack_require__("../../../../../src/app/common/toastr.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user_service__ = __webpack_require__("../../../../../src/app/user/user.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





function comparePassword(c) {
    var passwordControl = c.get('password');
    var confirmControl = c.get('retypepass');
    if (passwordControl.pristine || confirmControl.pristine) {
        return null;
    }
    if (passwordControl.value === confirmControl.value) {
        return null;
    }
    return { 'mismatchedPassword': true };
}
var RegisterComponent = (function () {
    function RegisterComponent(fb, userService, router, toastr) {
        this.fb = fb;
        this.userService = userService;
        this.router = router;
        this.toastr = toastr;
        this.firstname = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required]);
        this.lastname = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required]);
        this.email = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].email]);
        this.username = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].minLength(4), __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].maxLength(16)]);
        this.password = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required]);
        this.retypepass = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required]);
    }
    RegisterComponent.prototype.ngOnInit = function () {
        this.registerForm = this.fb.group({
            firstname: this.firstname,
            lastname: this.lastname,
            email: this.email,
            username: this.username,
            passwordGroup: this.fb.group({
                password: this.password,
                retypepass: this.retypepass,
            }, { validator: comparePassword })
        });
    };
    RegisterComponent.prototype.registerUser = function (formdata) {
        var _this = this;
        if (this.registerForm.dirty && this.registerForm.valid) {
            var theForm = this.registerForm.value;
            var thePass = this.registerForm.value.passwordGroup.password;
            theForm.password = thePass;
            delete theForm.passwordGroup;
            this.userService.register(theForm)
                .subscribe(function (data) {
                if (data.success === false) {
                    _this.toastr.error(data.message);
                }
                else {
                    _this.toastr.success(data.message);
                    _this.router.navigate(['login']);
                }
                _this.registerForm.reset();
            });
        }
    };
    RegisterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("../../../../../src/app/user/register.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_4__user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_3__common_toastr_service__["a" /* ToastrService */]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "../../../../../src/app/user/user.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__register_component__ = __webpack_require__("../../../../../src/app/user/register.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__profile_component__ = __webpack_require__("../../../../../src/app/user/profile.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__auth_guard_service__ = __webpack_require__("../../../../../src/app/user/auth-guard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__password_component__ = __webpack_require__("../../../../../src/app/user/password.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__logout_component__ = __webpack_require__("../../../../../src/app/user/logout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__auth_service__ = __webpack_require__("../../../../../src/app/user/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__user_service__ = __webpack_require__("../../../../../src/app/user/user.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var UserModule = (function () {
    function UserModule() {
    }
    UserModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["d" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* RouterModule */].forChild([
                    { path: 'register', component: __WEBPACK_IMPORTED_MODULE_4__register_component__["a" /* RegisterComponent */] },
                    { path: 'profile', canActivate: [__WEBPACK_IMPORTED_MODULE_6__auth_guard_service__["a" /* AuthGuard */]], component: __WEBPACK_IMPORTED_MODULE_5__profile_component__["a" /* ProfileComponent */] },
                    { path: 'password', canActivate: [__WEBPACK_IMPORTED_MODULE_6__auth_guard_service__["a" /* AuthGuard */]], component: __WEBPACK_IMPORTED_MODULE_7__password_component__["a" /* PasswordComponent */] },
                    { path: 'logout', canActivate: [__WEBPACK_IMPORTED_MODULE_6__auth_guard_service__["a" /* AuthGuard */]], component: __WEBPACK_IMPORTED_MODULE_8__logout_component__["a" /* LogoutComponent */] }
                ])
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__register_component__["a" /* RegisterComponent */],
                __WEBPACK_IMPORTED_MODULE_5__profile_component__["a" /* ProfileComponent */],
                __WEBPACK_IMPORTED_MODULE_7__password_component__["a" /* PasswordComponent */],
                __WEBPACK_IMPORTED_MODULE_8__logout_component__["a" /* LogoutComponent */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_9__auth_service__["a" /* AuthService */],
                __WEBPACK_IMPORTED_MODULE_6__auth_guard_service__["a" /* AuthGuard */],
                __WEBPACK_IMPORTED_MODULE_10__user_service__["a" /* UserService */]
            ]
        })
    ], UserModule);
    return UserModule;
}());



/***/ }),

/***/ "../../../../../src/app/user/user.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_do__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/do.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_observable_throw__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/throw.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var UserService = (function () {
    function UserService(http) {
        this.http = http;
        var theUser = JSON.parse(localStorage.getItem('currentUser'));
        if (theUser) {
            this.jwtToken = theUser.token;
        }
    }
    UserService.prototype.register = function (oUser) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.post('https://mean-address-book.herokuapp.com/register', JSON.stringify(oUser), options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    UserService.prototype.getUser = function (userid) {
        console.log('User id: ' + userid);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', "" + this.jwtToken);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get("https://mean-address-book.herokuapp.com/api/user/" + userid, options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    UserService.prototype.updateUser = function (userid, oUser) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', "" + this.jwtToken);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.put("https://mean-address-book.herokuapp.com/api/user/" + userid, JSON.stringify(oUser), options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    UserService.prototype.updatePassword = function (userid, oUser) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', "" + this.jwtToken);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.put("https://mean-address-book.herokuapp.com/api/password/" + userid, JSON.stringify(oUser), options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    UserService.prototype.handleError = function (error) {
        console.error(error);
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw(error.json().error || 'Server error');
    };
    UserService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map