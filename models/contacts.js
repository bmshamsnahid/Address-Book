var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ContactSchema = new Schema({
    fullName: { type: String, required: true },
    nickName: { type: String, required: true },
    phoneNumber: [Number],
    website: { type: String },
    dateOfBitrth: { type: Date },
    address: { type: String }
});

module.exports = mongoose.model('Contact', ContactSchema, 'Contact');