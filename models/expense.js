var mongoose = require('mongoose'),
    mongoosePaginate = require('mongoose-paginate'),
    Schema = mongoose.Schema;

var ExpenseSchema = new Schema({
    userid: { type: String, required: true },
    expenseData: { type: Date, required: true },
    expenseType: { type: String, required: true },
    expenseamt: { type: Number, required: true },
    expensedesc: { type: String }
});

ExpenseSchema.plugin(mongoosePaginate);


module.exports = mongoose.model('expenses', ExpenseSchema, 'expenses');
