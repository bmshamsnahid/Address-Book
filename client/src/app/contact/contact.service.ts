import {Injectable} from "@angular/core";
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

@Injectable()
export class ContactService {
  public jwtToken: string;

  constructor(private http: Http) {
    const theUser: any = JSON.parse(localStorage.getItem('currentUser'));
    if (theUser) {
      this.jwtToken = theUser.token;
    }
  }

  createContact(userId, contact) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `${this.jwtToken}`);
    const options = new RequestOptions({
      headers: headers
    });

    return this.http.post(`https://mean-address-book.herokuapp.com/api/contact/${userId}`, JSON.stringify(contact), options)
      .map((response: Response) => {
        return response.json();
      })
      .catch(this.handleError);
  }

  getSingleContact(userId, contactId) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `${this.jwtToken}`);
    const options = new RequestOptions({
      headers: headers
    });

    this.http.get('https://mean-address-book.herokuapp.com/api/contact/${userId}/${contactId}', options)
      .map((response: Response) => {
        return response.json();
      })
      .catch(this.handleError);
  }

  getAllContact(userId) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `${this.jwtToken}`);
    const options = new RequestOptions({
      headers: headers
    });

    return this.http.get(`https://mean-address-book.herokuapp.com/api/contact/${userId}`, options)
      .map((response: Response) => {
        return response.json();
      })
      .catch(this.handleError);
  }

  updateContact(userId, contactId, contact) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `${this.jwtToken}`);
    const options = new RequestOptions({
      headers: headers
    });

    return this.http.put(`https://mean-address-book.herokuapp.com/api/contact/${userId}/${contactId}`, JSON.stringify(contact), options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  deleteContact(userId, contactId) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `${this.jwtToken}`);
    const options = new RequestOptions({
      headers: headers
    });

    return this.http.delete(`https://mean-address-book.herokuapp.com/api/contact/${userId}/${contactId}`, options)
      .map((response: Response) => {
        return response.json();
      })
      .catch(this.handleError);
  }

  downloadCsv(userId) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `${this.jwtToken}`);
    const options = new RequestOptions({
      headers: headers
    });

    return this.http.get(`https://mean-address-book.herokuapp.com/api/csv-generator/${userId}`, options)
      .map((response: Response) => {
        return response;
      })
      .catch(this.handleError);
  }


  private handleError(error?: Response) {
    if (error) {
      console.log('Error in Contact Service: ' + error);
      return Observable.throw(error.json().error || 'Server Error');
    } else {
      console.log('Unknown err');
    }
  }
}
