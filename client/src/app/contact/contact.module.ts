import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {AuthGuard} from "../user/auth-guard.service";
import {ContactComponent} from "./contact.component";
import {DatePipe} from "@angular/common";
import {AuthService} from "../user/auth.service";
import {ContactService} from "./contact.service";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      { path: 'contact', canActivate: [AuthGuard], component: ContactComponent}
    ])
  ],
  declarations: [
    ContactComponent
  ],
  providers: [
    DatePipe,
    AuthService,
    AuthGuard,
    ContactService
  ]
})
export class ContactModule {}
