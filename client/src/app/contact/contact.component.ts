import {Component, ElementRef, OnInit} from "@angular/core";
import {AuthService} from "../user/auth.service";
import {ContactService} from "./contact.service";
import {FormBuilder} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {ToastrService} from "../common/toastr.service";
import {DatePipe} from '@angular/common'
import {Contact} from "./contact";
import {FormControl} from "@angular/forms";
import { FormsModule } from '@angular/forms';

@Component({
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  userObj: any;
  contacts: Contact[];
  currentContact: Contact;
  anotherNumber: any;

  newFullName: string;
  newNickName: string;
  newWebsite: string;
  phoneNumber: number[] = [];
  newNumber: any;
  newAddress: string;

  searchAddressKeyword: string;

  constructor(private authService: AuthService,
              private contactService: ContactService,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private toastr: ToastrService,
              private datePipe: DatePipe) {}

  ngOnInit() {
    this.currentContact = null;
    this.userObj =  this.authService.currentUser;
    console.log(this.userObj);
    this.getAllContacts();
  }

  onClickCreateContact() {
    console.log(this.newFullName);
    console.log(this.newNickName);
    console.log(this.newWebsite);
    console.log(this.newNumber);
    console.log(this.newAddress);
    this.newNumber = parseInt(this.newNumber);
    if (!this.newFullName || !this.newNickName || !this.newWebsite || !this.newNumber || !this.newAddress) {
      this.toastr.warning('Invalid or Incomplete Information');
      return;
    }
    if (this.newFullName.length <= 0 || this.newNickName.length <= 0 || this.newWebsite.length <= 0 || this.newNumber <= 0 || this.newAddress.length <= 0) {
      this.toastr.warning('Invalid or Incomplete Information');
      return;
    }

    var contact: Contact = new Contact();
    contact.fullName = this.newFullName;
    contact.nickName = this.newNickName;
    contact.website = this.newWebsite;
    contact.phoneNumber = this.newNumber;
    contact.address = this.newAddress;
    // contact.phoneNumber.push(this.newNumber);

    this.contactService.createContact(this.userObj.user.userid, contact).subscribe((data) => {
      if (data.success == true) {
        this.toastr.success('New contact created');
        console.log(data);
        this.contacts.push(data.contact);
        this.newFullName = null;
        this.newNickName = null;
        this.newWebsite = null;
        this.phoneNumber = null;
        this.newAddress = null;
      } else {
        this.toastr.error('Error in creating new contact: ' + data.message);
      }
    });


  }

  getAllContacts() {
    console.log('UserID: ' + this.userObj.user.userid);
    this.contactService.getAllContact(this.userObj.user.userid).subscribe((data) => {
      console.log(data);
      if (data.success == false) {
        this.toastr.error(data.message);
      } else {
        this.contacts = data.contacts;
        console.log(this.contacts);
      }
    });
  }

  onClickTable(contact): void {
    this.currentContact = contact;
  }

  onClickUpdateContact() {
    if (this.checkValidation(this.currentContact) == false) {
      this.toastr.warning('Invalid or Incomplete Information');
      console.log(this.currentContact);
    } else {
      this.contactService.updateContact(this.userObj.user.userid, this.currentContact._id, this.currentContact).subscribe((data) => {
        console.log(data);
        if (data.success == true) {
          this.toastr.success('Contacts Updated Successfully');
        } else {
          this.toastr.error(data.message);
        }
      });
    }
  }

  onClickDeleteContact() {
    this.contactService.deleteContact(this.userObj.user.userid, this.currentContact._id).subscribe((data) => {
      if (data.success == true) {
        this.toastr.success('Successfully removed the contact.');

        var updatedContactList: Contact[] = [];

        this.contacts.map((contact) => {
          if (contact._id != this.currentContact._id) {
            updatedContactList.push(contact);
          }
        });

        this.contacts = updatedContactList;

      } else {
        this.toastr.error('Error in deleting the contact: ' + data.message);
      }
    });
  }

  onClickSearchButton() {
    console.log('Searching: ' + this.searchAddressKeyword);
    if (!this.searchAddressKeyword) {
      this.toastr.warning('Invalid Search Keyword');
      return;
    } else {
      var newFilteredContactsList: Contact[] = [];
      this.contacts.map((contact) => {
        if (contact.address == this.searchAddressKeyword) {
          newFilteredContactsList.push(contact);
        }
      });
      if (newFilteredContactsList.length <= 0) {
        this.toastr.warning('No contacts found.');
      } else {
        this.contacts = newFilteredContactsList;
      }
    }
  }

  onclickDownloadAsCSV() {
    console.log('Download as CSV');
    this.contactService.downloadCsv(this.userObj.user.userid).subscribe((data) => {
      console.log(data);
      let parsedResponse = data.text();
      let blob = new Blob([parsedResponse], { type: 'text/csv' });
      let url = window.URL.createObjectURL(blob);

      if(navigator.msSaveOrOpenBlob) {
        navigator.msSaveBlob(blob, 'Book.csv');
      } else {
        let a = document.createElement('a');
        a.href = url;
        a.download = 'contacts.csv';
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
      }
      window.URL.revokeObjectURL(url);
    });
  }

  checkValidation(contact: Contact):boolean {
    if (!contact.fullName || !contact.nickName || !contact.website || contact.phoneNumber.length <= 0 || !contact.address) {
      this.toastr.warning('Incomplete or invalid information, undefined value');
      return false;
    }
    if (this.anotherNumber) {
      try {
        var newNumber = parseInt(this.anotherNumber);
        this.currentContact.phoneNumber.push(newNumber);
        this.anotherNumber = null;
      } catch (err) {
        this.toastr.warning('New number can not be added: ' + err);
      }
    }

    var validPhoneNumber: boolean = true;
    var filteredContactNumberList = [];

    contact.phoneNumber.map((value: any) => {
      try {
        if (value != null && value.toString().length > 0 && parseInt(value)) {
          filteredContactNumberList.push(parseInt(value));
        }
      } catch (err) {
        validPhoneNumber = false;
        this.toastr.warning('Invalid Phone Number: ' + value + ' is not a number');
      }
    });

    this.currentContact.phoneNumber = filteredContactNumberList;

    return validPhoneNumber;
  }

  trackByIndex(index: number, obj: any): any {
    return index;
  }
}
