export class Contact {
  _id?: string;
  fullName: string;
  nickName: string;
  phoneNumber: number[];
  website: string;
  dateOfBirth: Date;
  address: string;
}
