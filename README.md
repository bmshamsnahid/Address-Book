# [![MEAN Logo](http://mean.io/wp-content/themes/twentysixteen-child/images/meanlogo.png)](http://mean.io/) MEAN<sup>2</sup> Address Book

## [Video Screen Cast](https://youtu.be/CCPgf-K6WTU)
#### Contact Management Application with CSV exportation support.


## Prerequisite Technologies In Your Machine
* [Node.js](https://nodejs.org/en/)
* [Angular4 web starter](https://angular.io/)
* [MongoDB](https://www.mongodb.com)
* [Express](https://expressjs.com/)
* [Git](https://git-scm.com/downloads)
* [npm](https://www.npmjs.com/)

## Installation

To start your application with MEAN, you need to clone the base MEAN repository from Github. This repository contains all the packages, modules and also a sample code base in order to start and make it easy to develop your application. Following the steps below will guide you to install the latest MEAN version.

```
git clone https://github.com/bmshamsnahid/meanAddressBook.git  
cd meanAddressBook
npm install
node server.js
```
Open Another Terminal in root directory
```
cd client
npm install
ng serve
```