var express = require('express'),
    router = express.Router(),
    csvGeneratorController = require('../controller/csvGenerator');

router.get('/:userid', csvGeneratorController.generateCSV);

module.exports = router;