var express = require('express'),
    router = express.Router(),
    userController = require('../controller/user');

router.get('/user/:id', userController.authenticate, userController.getUserDetails);
router.put('/user/:id', userController.authenticate, userController.updateUser);
router.put('/password/:id', userController.authenticate, userController.updatePassword);

module.exports = router;