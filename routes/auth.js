var express = require('express'),
    router = express.Router(),
    authController = require('../controller/auth');

router.post('/login', authController.login);
router.post('/register', authController.signup);

module.exports = router;