var express = require('express'),
    router = express.Router(),
    contact = require('../models/contacts'),
    User = require('../models/user'),
    contactController = require('../controller/contact'),
    userController = require('../controller/user');

router.post('/:userid', userController.authenticate, contactController.createContact);
router.get('/:userid/:contactid', userController.authenticate, contactController.getContact);
router.get('/:userid/', userController.authenticate, contactController.getAllContacts);
router.put('/:userid/:contactid', userController.authenticate, contactController.updateContact);
router.delete('/:userid/:contactid', userController.authenticate, contactController.deleteContact);

module.exports = router;