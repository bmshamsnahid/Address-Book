var mongoose = require('mongoose'),
    User = require('../models/user'),
    jwt = require('jsonwebtoken'),
    config = require('../config');

var authenticate = (req, res, next) => {
    var token = req.body.token || req.query.token || req.headers['authorization'];
    if (token) {
        jwt.verify(token, config.secret, (err, decoded) => {
            if (err) {
                return res.status(400).json({
                    success: false,
                    message: 'Fatal server error: ' + err
                });
            } else {
                req.decoded = decoded;
                next();
            }
        })
    } else {
        return res.status(400).json({
            success: false,
            message: 'Fatal error, token is not available.',
            errorCode: 'no-token'
        });
    }
};

var getUserDetails = (req, res, next) => {
    User.find({_id: req.params.id}, (err, user) => {
        if (err) {
            return res.status(400).json({
                success: false,
                message: 'Fatal server error: ' + err
            });
        } else {
            return res.status(201).json({
                success: true,
                data: user
            });
        }
    });
};

var updateUser = (req, res, next) => {
    var firstname = req.body.firstname,
        lastname = req.body.lastname,
        email = req.body.email,
        userid = req.params.id;

    if (!firstname || !lastname || !email || !userid) {
        return res.status.json({
            success: false,
            message: 'Posted data incomplete'
        });
    } else {
        User.findById(userid, (err, user) => {
            if (err) {
                return res.status(400).json({
                    success: false,
                    message: 'Fatal server error: ' + err
                });
            }
            if (user) {
                user.firstname = firstname;
                user.lastname = lastname;
                user.email = email;
            }
            user.save((err) => {
                if (err) {
                    return res.status(400).json({
                        success: false,
                        message: 'Fatal server error: ' + err
                    });
                }
                return res.status(200).json({
                    success: true,
                    message: 'User details updated'
                });
            });
        });
    }
};

var updatePassword = (req, res, next) => {
    const userid = req.params.id;
    const oldpassword = req.body.oldpassword;
    const password = req.body.password;

    if (!oldpassword || !password || !userid) {
        return res.status(422).json({ success: false, message: 'Posted data is not correct or incompleted.'});
    } else {

        User.findOne({ _id: userid }, function(err, user) {
            if(err){ res.status(400).json({ success: false, message:'Error processing request '+ err}); }
            if (user) {
                user.comparePassword(oldpassword, function (err, isMatch) {
                    if (isMatch && !err) {

                        user.password = password;

                        user.save(function(err) {
                            if(err){ res.status(400).json({ success: false, message:'Error processing request '+ err}); }

                            res.status(201).json({
                                success: true,
                                message: 'Password updated successfully'
                            });
                        });
                    } else {
                        res.status(201).json({ success: false, message: 'Incorrect old password.' });
                    }
                });
            }
        });
    }
};

module.exports = {
    authenticate,
    getUserDetails,
    updateUser,
    updatePassword
};
