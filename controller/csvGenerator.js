var json2csv = require('json2csv');
var fs = require('fs');
var Contact = require('../models/user');
var User = require('../models/user');
var path = require('path');

var generateCSV = (req, res, next) => {
    var fields = ['fullName', 'nickName', 'phoneNumber', 'website', 'address'];
    // var myCars = [
    //     {
    //         "car": "Audi",
    //         "price": 40000,
    //         "color": "blue"
    //     }, {
    //         "car": "BMW",
    //         "price": 35000,
    //         "color": "black"
    //     }, {
    //         "car": "Porsche",
    //         "price": 60000,
    //         "color": "green"
    //     }
    // ];

    var userId = req.params.userid;

    if (!userId) {
        return res.status(201).json({
            success: false,
            message: 'Invaild or Incomplete information'
        });
    }

    User.findById(userId, (err, user) => {
        if (err) {
            return res.status(400).json({
                success: false,
                message: 'Fatal server err: ' + err
            });
        } else {
            var csv = json2csv({ data: user.contacts, fields: fields });
            fs.writeFile('public/csvFiles/file.csv', csv, function(err) {
                if (err) {
                    return res.status(400).json({
                        success: false,
                        message: 'Fatal server err: ' + err
                    });
                } else {
                    res.download(path.join(__dirname, '../public/csvFiles/file.csv'));
                }
            });
        }
    });
};

module.exports = {
    generateCSV
};