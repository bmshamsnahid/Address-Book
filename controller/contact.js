var mongoose = require('mongoose'),
    User = require('../models/user'),
    Contact = require('../models/contacts');

var createContact = (req, res, next) => {
    var userId = req.params.userid,
        fullName = req.body.fullName,
        nickname = req.body.nickName,
        phoneNumber = req.body.phoneNumber,
        website = req.body.website,
        dateOfBirth = req.body.dateOfBitrth,
        address = req.body.address;

    if (!userId || !fullName || !phoneNumber || !address) {
        return res.json(201).json({
            success : false,
            message: 'Incomplete contact information'

        });
    } else {
        User.findById(userId, (err, user) => {
            if (err) {
                return res.status(400).json({
                    success: false,
                    message: 'Fatal server err: ' + err
                });
            } else {
                var userContacts = user.contacts;
                isNumberExist(user.contacts, phoneNumber, req, res, next, (err, req, res, next) => {
                    if (err) {
                        return res.status(201).json({
                            success : false,
                            message: 'Contacts Not Saved: ' + err

                        });
                    } else {
                        var contact = new Contact({
                            fullName: fullName,
                            nickName: nickname,
                            website: website,
                            dateOfBitrth: dateOfBirth,
                            address: address
                        });
                        contact.phoneNumber.push(phoneNumber);
                        user.contacts.push(contact);
                        user.save((err) => {
                            if (err) {
                                return res.status(400).json({
                                    success: false,
                                    message: 'Fatal server err: ' + err
                                });
                            } else {
                                return res.status(201).json({
                                    success: true,
                                    message: 'Contact Saved Successfully',
                                    contact: contact
                                });
                            }
                        });
                    }
                });
            }
        });
    }
};

var getContact = (req, res, next) => {
    var userId = req.params.userid,
        contactsId = req.params.contactid;

    if (!userId || !contactsId) {
        return res.status(201).json({
            success: false,
            message: 'Invalid Information'
        });
    }

    User.findById(userId, (err, user) => {
        if (err) {
            return res.status(400).json({
                success: false,
                message: 'Fatal server err: ' + err
            });
        } else {
            isContactExist(user.contacts, contactsId, req, res, next, (contact, req, res, next) => {
                if (contact) {
                    return res.status(201).json({
                        success: true,
                        message: 'Contacts found',
                        contact: contact
                    });
                } else {
                    return res.status(201).json({
                        success: false,
                        message: 'Contact Not Found'
                    });
                }
            });
        }
    });
};

var getAllContacts = (req, res, next) => {
    var userId = req.params.userid;

    if (!userId) {
        return res.status(201).json({
            success: false,
            message: 'Invaild or Incomplete information'
        });
    }

    User.findById(userId, (err, user) => {
        if (err) {
            return res.status(400).json({
                success: false,
                message: 'Fatal server err: ' + err
            });
        } else {
            return res.status(201).json({
                success: true,
                message: 'Successfully get all the contacts',
                contacts: user.contacts
            });
        }
    });
};

var updateContact = (req, res, next) => {
    var userId =  req.params.userid,
        contactId = req.params.contactid,
        fullName = req.body.fullName,
        phoneNumber = req.body.fullName,
        address = req.body.address;

    if (!userId || !contactId || !fullName || !phoneNumber || !address) {
        return res.status(201).json({
            success: false,
            message: 'Invalid or Incomplete information'
        });
    }

    User.findById(userId, (err, user) => {
        if (err) {
            return res.status(201).json({
                success: false,
                message: 'Invalid User'
            });
        } else {
            updateContactValue(user.contacts, contactId, req, res, next, (contacts, req, res, next) => {
                user.contacts = contacts;
                user.save((err, updatedUser) => {
                    if (err) {
                        return res.status(400).json({
                            success: false,
                            message: 'Fatal Server Error: ' + err
                        });
                    } else {
                        return res.status(201).json({
                            success: true,
                            message: 'Successfully updated the contacts',
                            user: updatedUser
                        });
                    }
                });
            });
        }
    });
};

var deleteContact = (req, res, next) => {
    var userId = req.params.userid,
        contactId = req.params.contactid;

    if (!userId || !contactId) {
        return res.status(201).json({
            success: false,
            message: 'Invalid or Incomplete information'
        });
    }

    User.findById(userId, (err, user) => {
        if (err) {
            return res.status(400).json({
                success: false,
                message: 'Fatal Server Error: ' + err
            });
        } else {
            removeContact(user.contacts, contactId, req, res, next, (contacts, req, res, next) => {
                user.contacts = contacts;
                user.save((err, updatedUser) => {
                    if (err) {
                        return res.status(400).json({
                            success: false,
                            message: 'Fatal Server Error: ' + err
                        });
                    } else {
                        return res.status(201).json({
                            success: true,
                            message: 'Contact removed successfully',
                            user: updatedUser
                        });
                    }
                });
            });
        }
    });
};

module.exports = {
    createContact,
    getContact,
    getAllContacts,
    updateContact,
    deleteContact
};

var isNumberExist = (contacts, number, req, res, next, cb) => {
    contacts.map((contact) => {
        if (contact.phoneNumber == number) {
            return cb('Number Already Exists', req, res, next);
        }
    });
    return cb(null, req, res, next);
};

var isContactExist = (contacts, contactsId, req, res, next, cb) => {
    contacts.map((contact) => {
        if (contact._id == contactsId) {
            return cb(contact, req, res, next);
        }
    });
    return cb(null, req, res, next);
}

var updateContactValue = (contacts, contactid, req, res, next, cb) => {
    contacts.map((contact) => {
        if (contact._id == contactid) {
            contact.fullName = req.body.fullName;
            contact.nickName = req.body.nickName || contact.nickName;
            contact.phoneNumber = req.body.phoneNumber;
            contact.website = req.body.website || contact.website;
            contact.dateOfBitrth = req.body.dateOfBitrth || contact.dateOfBitrth || '';
            contact.address = req.body.address || contact.address;
            return contact;
        }
    });
    return cb(contacts, req, res, next);
}

var removeContact = (contacts, contactId, req, res, next, cb) => {
    var newContacts = [];
    contacts.map((contact) => {
        if (contact._id != contactId) {
            newContacts.push(contact);
        }
    });

    console.log(newContacts);
    return cb(newContacts, req, res, next);
}
