var mongoose = require('mongoose'),
    User = require('../models/user'),
    jwt = require('jsonwebtoken'),
    config = require('../config');

var signup = (req, res, next) => {
    var firstname = req.body.firstname,
        lastname = req.body.lastname,
        email = req.body.email,
        username = req.body.username,
        password = req.body.password;

    if (!firstname || !lastname || !email || !username || !password) {
        return res.status(422).json({
            success: false,
            message: 'Posted data incomplete'
        });
    }

    User.findOne({username: username}, (err, existingUser) => {
        if (err) {
            return res.status(400).json({
                success: false,
                message: 'Fatal server err: ' + err
            });
        }

        if (existingUser) {
            return res.status(201).json({
                success: false,
                messgae: 'Username already exist'
            });
        }

        var oUser = new User({
            firstname: firstname,
            lastname: lastname,
            email: email,
            username: username,
            password: password
        });

        oUser.save((err, oUser) => {
            if (err) {
                return res.status(400).json({
                    success: false,
                    message: 'Fatal server error: ' + err
                });
            }
            return res.status(201).json({
                success: true,
                messgae: 'User created successfully, create an account'
            });
        });
    });
};

var login = (req, res, next) => {
    User.findOne({username: req.body.username}, (err, user) => {
        console.log('Username: ' + req.body.username);
        console.log('user: ' + user);
        if (err) {
            return res.status(400).json({
                success: false,
                message: 'Fatal server error: ' + err
            });
        }
        if (!user) {
            return res.status(201).json({
                success: false,
                message: 'Incorrect login credential'
            });
        } else if (user) {
            user.comparePassword(req.body.password, (err, isMatch) => {
                if (isMatch && !err) {
                    var token = jwt.sign(user, config.secret, {
                        expiresIn: config.tokenexp
                    });
                    user.lastLogin = new Date();
                    user.save((err) => {
                        if (err) {
                            return res.status(400).json({
                                success: false,
                                message: 'Fatal server error: ' + err
                            });
                        }

                        return res.status(201).json({
                            success: true,
                            message: {
                                'userid': user._id,
                                'username': user.username,
                                'firstname': user.firstname,
                                'lastname': user.lastname,
                                'lastLogin': user.lastLogin
                            },
                            token: token
                        });
                    });
                } else {
                    return res.status(201).json({
                        success: false,
                        message: 'Incorrect login credentials. ' + err
                    });
                }
            });
        }
    });
};

module.exports = {
    signup,
    login
};